# Frankenstein

- horrorový gotický román
  - (návaznost na Poea?)
- inspirace mýtem o Prométheovi

## Shrnutí děje

Evropa v 18./19. století – Švýcarské Alpy

Robert Walton píše dopisy své sestře do Anglie. Sám je na cestách a koná přípravy pro plavbu k severnímu pólu. Zmiňuje se o průběhu své cesty a píše o záhadném muži, jemuž on a jeho posádka zachránili život, když plul na malé kře nedaleko pólu. Muž se postupně uzdravuje a vypráví kapitánu Waltonovi svůj hrůzostrašný příběh. Jmenuje se Viktor Frankenstein a pochází z Ženevy. Když mu v mládí zemřela matka, rozhodl se, že půjde na univerzitu do Ingolstadtu. Zde se seznámil s Jindřichem Clervalem, jeho pozdějším nejlepším přítelem. Viktor miluje vědu a je úplně unesen přednáškami jistého profesora Waldmana, který věří, že život může být stvořen z neživých látek. Viktor se o to pokusí. Dlouho dobu sbírá materiál na vytvoření bytosti, propočítává různé věci, až jedné noci se mu povede sestavené monstrum oživit. Bytost je daleko větší než lidé a má škaredý a děsivý obličej. Když si Frankenstein uvědomí, co provedl, je už pozdě. Netvor sice uteče, ale znovu se shledávají poté, co Frankenstein odcestuje zpět do své rodné Ženevy kvůli záhadné smrti svého nejmladšího bratra Viléma. Z vraždy je obviněna dlouholetá přítelkyně Frankensteinovy rodiny Justýna Moritzová, která je sice nevinná, ale i přesto je popravena. Viktor se jednoho dne vydá na výlet k ledovci, kde se setkává s netvorem. Chce se mu pomstít za smrt svého bratra a vlastně i Justýny, která z ní byla obviněna. Netvor ho však přemluví, aby ho nejprve vyslyšel, než cokoliv udělá. Jdou spolu do jeskyně a netvor vypráví o počátku svého života, jak trpěl hlady a žízní, popisuje své pocity a myšlenky i to, jak se setkal s lidmi, kteří ho bili a on nevěděl proč. Ukryl se v kůlně jedné rodiny a díky pozorování se postupně naučil mluvit a psát. Byli to moc hodní, milí a láskyplní lidé, že on sám si myslel, že by byli schopni mít snad i jeho rádi. Dlouho se připravoval na to jak na ně zapůsobit, až se jeden den rozhodl a šel zaklepat na jejich dveře. Mluvil se starcem, který však byl slepý. Vypadalo to nadějně, dokud nepřišly starcovy děti. Syn začal netvora bít a ten raději utekl. Byl velmi naštvaný na ty lidi i na sebe, ale největší nenávist směřovala proti svému stvořiteli. Netvor ho chtěl vypátrat. Jednoho dne potkal blízko Ženevy malého chlapce, byl to Viktorův bratr. Jakmile to netvor zjistil, vzpomněl si na svého stvořitele a chlapce zabil. Jakmile si uvědomil, co provedl, bylo už pozdě. Utekl do hor, kde se později potkal se svým stvořitelem Frankensteinem. Tímto dokončil svoje vyprávění a dodal prosbu. A to takovou, že je na světě sám, nemá se s kým bavit a tak že by si přál ženu stejně tak velkou a škaredou jako je on sám. Potom že dá pokoj Viktorovi i celému lidstvu a odejde do pustých krajin. Viktor nad tím uvažuje a potom slíbí, že tak tedy učiní. Vydává se i se svým přítelem Jindřichem do Anglie a potom i do Skotska. V jedné odlehlé vesničce se pustí do své příšerné práce a vytvoří dalšího tvora. Jenomže si před jeho oživením uvědomí, že to asi není dobrý nápad a tvora zničí. Netvor to zjistí a rozzlobí se na Viktora. Později zabije jeho přítele Jindřicha a po návratu domů a svatbě s Alžbětou i ji. Nato žalem zemře i Viktorův otec. Sám Viktor je na dně, nejraději by sám také zemřel, ale chce se nejprve pomstít a netvora zničit. Vydá se ho hledat a setkají se až v ledových pustinách severního pólu. Ledy se však zlomí a Viktor se ocitá na kře, kde by se málem utopil, kdyby ho nezachránil kapitán Walton. Tím dopoví Viktor Frankenstein svůj příběh a za krátký čas zemře nemocí a vysílením. Předtím však poprosí kapitána Waltona o to, že kdyby se s netvorem setkal, aby ho zabil. To však nebude třeba. Netvor se objeví na lodi a když zjistí, že je jeho stvořitel mrtvý, sám se rozhodne, že skončí se svým životem. Kapitán je toho všeho svědkem. Severního pólu nedosáhne a vrátí se zpět do Anglie.

## Kompozice

- retrospektivní pasáže vyprávěné netvorem, jinak lineární
- 3 vypravěči (v ich formě) – Robert Walton, Viktor Frankenstein, netvor
  - Walton píše dopisy o tom, co mu Frankenstein vypověděl

## Postavy

- **Viktor Frankenstein** – nadšený vědec zaslepený pokrokem, nejistý ale spravedlivý
- **Robert Walton** – kapitán výpravy na severní pól, do příběhu je vlastně zavlečen mimoděk
- **Netvor** – velmi nešťastný a plný hněvu, pomstychtivý, nechápe, proč se ho lidé bojí, nezná rozdíl mezi dobrem a zlem, touží po pochopení a po lásce
- **Alžběta** – milující a obětavá, nevlastní sestra (následně manželka, eew) Viktora, snaží se všem za každou cenu pomoci

## Historický kontext

- romantismus
  - popisy krajin a počasí
  - background postav, jejich rodinné zázemí, etc.
  - důraz na cit, prožitek a individualitu
  - reakce na přehnanou rozumovost osvícenství, inspirace v antice a klasicismu

- *Karel Hynek Mácha*, *Jane Austen*, *Lord Byron*

### Mary Shelley

- manželka Percyho Shelley – tehdejší známý spisovatel
- dcera známe feministky a slavného anarchistického filosofa

- *The Last Man*, *The Fortunes of Perkin Warbeck*

## Intertexty

Nepochopení, touha po inkluzi

- O myších a lidech (postava Lennieho) – John Steinbeck
- Kdo chytá v žitě – J. D. Salinger

Horor

- Povídky – Edgar Alan Poe

Stvoření monstra

- Podivný případ Dr. Jekylla a pana Hyda – R. L. Stevenson
- Mýtus o Prométheovi (stvořil člověka tím, že mu přinesl oheň)
