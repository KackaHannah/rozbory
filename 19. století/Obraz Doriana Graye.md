# Obraz Doriana Graye

## Shrnutí děje

Příběh začíná v ateliéru Basila Hallwarda, kde rozmlouvá se svým přítelem Henrym o úžasném mladíkovi - Dorianovi. Basil namaloval Dorianův portrét, nad kterým Dorian vyřkl prosbu, aby mohl ten obraz stárnou místo něj a on sám si zachoval svou současnou podobu, už tuto myšlenku mu vnukl lord Henry. Postupem času se z nich stanou přátelé a lord Henry má na Doriana obrovský vliv. Basil si uvědomuje nebezpečnost Harryho vlivu a myšlenek na nevinného Doriana, ten jeho naléhání přesto neuposlechne. Dorian se zamiluje do mladé krásné herečky Sibyly, která je jím okouzlena. Když se rozhodne ji ukázat svým přátelům, je znechucen, jak špatně ten večer hrála, přestože mu to Sibyla vysvětlí, on ji krutým způsobem odmítne a ona spáchá sebevraždu. Když si druhý den uvědomí, co udělal a chce ji zpět, dozví se o její smrti - toto je první událost, která poznamená jeho obraz.
Když ho Dorian uvidí, zalekne se a ukryje jej na půdu. Uběhne mnoho let a o Dorianovi se povídá, jak je krutý a bezcitný; a je to pravda. Dorianův portrét se stále více mění, má krutý výraz ve tváři a stárne, zatímco Dorian zůstává stále stejný. Když na Dorianovi třicáté osmé narozeniny přijde na návštěvu Basil, přiměje Doriana, aby mu obraz ukázal. Dorian je nešťastný, načež ho zachvátí vztek a Basila zabije. Zavolá na pomoc svého bývalého přítele, vědce a chemika Alana Cambella, a pod výhrůžkami ho donutí zničit Basilovo tělo beze stop. Alan sám později spáchá sebevraždu. Když se Dorian zamiluje do krásné vesničanky, raději ji opustí, aby jí nezkazil život. Jenže Henry ho přesvědčí o tom, že už to stejně udělal, načež Dorian zkontroluje obraz a zjistí, že je opravdu ještě více zkažený. Neunese to, rozhodne se polepšit a obraz zničit. Vezme stejný nůž, kterým zabil Basila, a zabodne ho do obrazu, jenže on a obraz byli natolik propojeni, že zabije i sebe. Když jej najde služebnictvo, nemohou ho poznat, stal se z něj ošklivý stařec, kdežto na obrazu je onen krásný mladík.

## Kompozice

- chronologická
- popisy anglické aristokratické společnosti
  - autor se vyžívá v dlouhých popisech interiérů a dialozích (take a shot for each "gobelín")

## Postavy

- **Dorian Gray** – zprvu nevinný mladý chlapec, postupně se dává na dráhu drog, žen na jednu noc a prostě dekadentního života, zprvu zamilován do hereckého talentu Sibyly
- **Lord Henry Wotton** – manipulativní dekadent, přesto oblíben ve společnosti
- **Basil Hallward** – okouzlen krásou Doriana (zamilován?), nedokáže se dívat na to, jakým směrem se Dorian vydal
- **Sibyla Vaneová** – nadaná herečka, zamilovává se do Doriana a ztrácí tím svůj herecký talent a v návaznosti na to i přízeň Doriana, spáchá kvůli tomu sebevraždu
- **James Vane** – bratr Sibyly, chce pomstít její smrt

## Historický kontext

- moderna
  - symbolismus, dekadence
  - umění pro umění
- *Rimbaud a Verlaine*
- *Buřiči – Gellner, Dyk, …*

### Oscar Wilde

- prvotřídní dekadent :sparkles:
  - homosexualita – román použit jako důkaz u soudu
  - provokuje puritánskou společnost svými dekadentními názory a oblékáním

- *Salome*, *Lady Fuckingham* (asi, neví se)

## Intertexty

Dekadence
- **Naruby** (Lord Henry tuto knihu dokonce Dorianovi na začátku dá) – Huysmans

Odosobnění od zla
- Podivný případ Dr. Jekylla a pana Hyda – R. L. Stevenson

Nešťastná láska
- Utrpení mladého Werthera (postava Basila i Sibyly) – Goethe

Manipulace
- Spalovač mrtvol (manipulace duševně slabším Kopferkinglem) – Ladislav Fuks

Nepromyšlené přání, zaprodání duše

- Faust – Marlow, Goethe, Mann
