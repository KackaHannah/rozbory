# Podivný případ Dr. Jekylla a pana Hyda

## Shrnutí děje

19. století, Londýn

V Londýně se objevil podivný člověk, ze kterého čiší hrůza a v každém vzbuzuje neklidné pocity a nechuť. Zvláště rozrušený je pak advokát Utterson, kterého znepokojila závěť jeho dobrého přítele Henryho Jekylla. V případě jeho smrti či zmizení má veškerý majetek dostat právě Edward Hyde. Vše má prostý důvod – doktoru Jekyllovi se podařilo namíchat směs, jakousi drogu, po jejímž požití se, tělem i duší, změní v Hyda. To mu dovoluje oddávat se nočnímu životu a choutkám, které si jako vážený doktor nemůže dovolit. Hyde však postupem času nabývá na síle a svévolně přebírá vládu a Jekyllovi ke všemu začnou docházet látky potřebné k namíchání lektvaru. Zoufale se snaží jej znovu namíchat, aby mohl držet Hyda na uzdě, ale původní chemikálie byly pravděpodobně znečištěné a stejný vzorek se mu nedaří sehnat. Zanechá dopis, ve kterém vše vysvětluje a když zjistí, že se již nedokáže přeměnit zpět v Jekylla, spáchá sebevraždu.

## Kompozice

- 2 části
  1. Utterson podniká vyšetřování záhady kolem pana Hyda
  2. objasňující dopis od Dr. Jekylla
  - s oběma částmi jsou spojeni různí vypravěči 

## Postavy

- **Gabriel Utterson** – protagonista, vysoce inteligentní, profesionální a bez emocí, zároveň velmi starostlivý
- **Dr. Lanyon** – milý, srdečný a starostlivý, dobrý přítel Jekylla
- **Dr. Henry Jekyll** – talentovaný vědec, citlivý, ctěný a vážený, oblíbený, ztělesněné dobro
- **pan Hyde** – antagonista, cholerik, krutý, ztělesněné zlo

## Historický kontext

- novoromantismus
  - důraz na lidskou psychiku a prožitek
  - nadpřirozené elementy

- E. A. Poe, Julius Zeyer, Jaroslav Vrchlický

- Sir Arthur Conan Doyle
  - *příběhy o Sherlocku Holmesovi*
  - fantastické povídky, detektivky


### R. L. Stevenson

- 1850–1894
- skotský novelista, básník a cestopisec
- nejvíce píše romány a fantastické příběhy
- celoživotně velmi nemocný (chronická Bronchitida)
- fun fact: J&H napsal během asi 4 dní, kdy byl prostě jen permanentně sjetej na kokainu

- *Ostrov Pokladů*

## Intertexty

Dobro a zlo

- Obraz Doriana Graye – Oscar Wilde
- Zločin a trest – F. M. Dostojevskij

Stvoření mostra

- Frankenstein – Mary Shelley

Žánr

- Povídky o Sherlocku Holmesovi – A. C. Doyle
- Sud vína amontilladského – E. A. Poe
