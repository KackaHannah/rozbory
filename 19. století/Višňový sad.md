# Višňový sad

## Shrnutí děje

Ljubov Andrejevna Raněvská se s dcerou Aňou vrací z Paříže, kam odjely po tragické smrti syna Gríši. Návrat na rodné sídlo v ní probudil sentimentální myšlenky. Vrací se z důvodu nedostatku financí. Statek je natolik zadlužen, že je jen kousek před krachem i přesto, že se o něj svědomitě stará Varja, nevlastní dcera Raněvské.

Statek má být prodán i s krásným višňovým sadem, proto Varjin nápadník, podnikatel Lopachin, radí pronajmout pozemky sadu chatařům, ale jeho návrh se Raněvské a jejímu bratrovi Gajevovi vůbec nezamlouvá, odmítají rozprodat svou minulost. Na dražbě panství překvapivě koupí Lopachin a nechává višňový sad vykácet.

Panovačná služebná Duňaša, odmítla již několik nabídek ke sňatku, ale zamilovala se do mladíka Jepichodova, prožívají krátký vztah, ale nakonec ji Jepichodov opustí kvůli své kariéře. Rodina se rozpadá, všem se změní jejich život. Varja se rozchází s Lopachinem, protože o ni již Lopachin nemá zájem. Raněvská a Gajev se, spolu s komorníkem Jašou, vracejí zpět do Paříže. Jaša se díky tomu zbaví panské Duňaši, a naivní Aňu láká nový život, respektive změna života.

Péťa Trofimov, student a bývalý učitel zesnulého Gríši, stejně jako Aňa věří, že Lopachin koupil panství jen dočasně. Hru ukončuje monolog nemocného komorníka Firse. Nevšimli si, že zůstal v domě určeném k demolici a tak zapomenut může jen čekat na nevyhnutelný konec.

## Kompozice

- 4 jednání

- lyrické drama
  - příběh tam je, ale vždycky se odehraje "za scénou"
  - postavy se pak vždy jen zpětně baví o tom, co že se to vlastně stalo
- vnitřní monology postav
  - spíš než aby probíhaly rozhovory, postavy si tak trochu mluví pro sebe
  - řešení smyslu života

## Postavy

- **Raněvská** – majitelka panství, odmítá se rozloučit s vlastním sadem a přijmout realitu, sentimentální až staromódní
- **Lopachin** – dříve chudý, nyní lstivý podnikatel a oportunista, který je schopen pro vlastní zisk udělat naprosto cokoliv
- **Varja** – adoptivní dcera Raněvské, snaží se starat o panství, chce vstoupit do kláštera nebo se provdat za Lopachina
- **Aňa** – dcera Raněvské, naivně očekává změnu od přestěhování se do města
- **Gajev** – bratr Raněvské, cítí se provinile, že je bez práce a nevydělává na panství
- **Firs** – velmi oddaný starý komorník, ostatní ho berou trochu jako samozřejmost a nevšímají si ho
- **Jaša** – povýšenecký komorník
- **Jepichodov** – účetní
- **Duňaša** – pokojská

## Historický kontext

- přelom století – sociálně-politické problémy
  - sílící snahy středních vrstev a dělnického hnutí
  - snaha zrušit nevolnictví
  - rostoucí odpor neruských národů (Poláci, Litevci, Finové, …)
- obraz zániku nižší ruské šlechty a starého Ruska
- rozmach ruského Realismu
  - Tolstoj, Gorký, Puškin, Gogol

### A. P. Čechov

- ruský dramatik a prozaik
- původně vystudoval medicínu
- Višňový sad je poslední dílo autora
  - těžce nemocný tuberkulózou

- *Racek*, *Tři sestry*

## Intertexty

Lakomství a pokrytectví

- Lakomec (Harpagon = Lopachin) – Molliére

Romantizace venkova

- Krysař – Vojtěch Dyk

Hledání smyslu života / odmítnutí změny

- Hamlet – Shakespeare
- Kdo chytá v žitě – Salinger

