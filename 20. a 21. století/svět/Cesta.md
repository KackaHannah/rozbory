# Cesta

## Shrnutí děje

- USA blízko pobřeží, postapokalyptická budoucnost

Cesta sleduje putování bezejmenného otce a syna na jejich pouti k moři napříč postapokalyptickou krajinou. Několik let po blíže neurčené globální katastrofě je civilizace i většina života na Zemi zničena. Krajina je nehostinná a bezútěšná: Slunce je zakryto všudypřítomným popílkem, kvůli kterému vyhynula veškerá vegetace, a i hlavní hrdinové se před ním chrání rouškami. Zbytky lidskosti jsou vytlačovány bezohlednou krutostí a kanibalismem. S vědomím, že další zimu by na stejném místě nepřežili, vede muž svého syna skrze zničenou krajinu směrem k moři, udržující matnou naději, že se setkají s dalším „dobrými lidmi“.

Ještě před začátkem románu spáchá pod vlivem zoufalé a zřejmě beznadějné situace chlapcova matka (která otěhotněla ještě před katastrofou) sebevraždu. Každé ráno otec vykašlává krev a uvědomuje si, že umírá. Snaží se však svého syna ochránit před neustávající hrozbou napadení, podchlazení či vyhladovění, a také před synovou vlastní bezelstnou, avšak nebezpečnou touhou pomáhat každému poutníkovi, kterého potkají. Přechovává revolver se dvěma náboji – buď na ochranu nebo, kdyby to bylo potřeba, aby mohli spáchat sebevraždu. Tváří v tvář všem překážkám jsou jeden druhému celým světem. Otec si udržuje představu – a syn skutečně věří – že v hloubi lidskosti zbyl etický rozměr. Neustále jeden druhého ujišťují, že oni jsou „dobří lidé“, kteří „nesou oheň“.

Na své pouti se dvojice potýká s neustálým nedostatkem jídla, potulnými bandami kanibalů či nevýslovnými hrůzami jako dítě opečené na rožni či lidští zajatci udržovaní při životě, zatímco jsou jejich končetiny postupně zpracovávány na jídlo. Když na samém konci knihy dorazí po velkých útrapách na jih, aniž by se dočkali hledané spásy, otec podlehne své nemoci a umírá a syna zanechá opuštěného na ulici. Po třech dnech nalezne truchlícího kluka muž, který dvojici stopoval. Muž přijme chlapce do rodiny a zavede ho ke své ženě a dvěma dětem. Jedno z jeho dětí je děvče, a tak zůstává navzdory nepříznivým okolnostem naděje na budoucnost pro lidstvo. Krátký epilog hloubá nad přírodou a nekonečnem v měnícím se prostředí.

## Kompozice

- nadosobní vypravěč, nejprve otec, poté i chlapec
- neznačená přímá řeč – absence uvozovek
- jednoduché věty, jazyková strohost
- bezejmenné postavy – Kafka

## Postavy

- **Otec** – milující otec, syn mu dává důvod žít, optimistický, obětavý, odmítá se vzdát, zabývá se jen a pouze přežitím
- **Syn** – tichý, chytrý, dobrosrdečný, stará se o svého otce, lhostejný vůči hrůzám okolo, nezažil světem před apokalypsou
- **Matka** – objevuje se jen ve vzpomínkách, spáchala sebevraždu

## Historický kontext

- postmoderna
  - pluralita názorů a jejich zrovnoprávnění
  - zpochybnění optimistického pohledu na vývoj západní civilizace a dějin
    - ochabnutí zájmu o funkcionalismus a další avantgardní hnutí
  - Jean-François Lyotard – významný filosof hnutí

- Lyotard – *O postmodernismu*
- Umberto Eco – *Jméno růže*
- Murakami – *Konec světa a Hard-Boiled Wonderland*
- Nabokov – *Lolita*
- Kundera – *Žert*

### Cormac McCarthy

- rigidní vzdělání amerického Jihu
- v románech popisuje hodně násilí
  - návaznost na dystopie
- *Tahle země není pro starý* – thriller
- *Krvavý poledník* – kronika amerického Jihu

## Intertexty

(Post) apokalypsa

- Den Trifidů – John Wyndham
- Apokalypsa – Bible

Cesta

- Exodus – Bible
- Odyssea – Homér
- Hobit, Pán Prstenů – Tolkien





