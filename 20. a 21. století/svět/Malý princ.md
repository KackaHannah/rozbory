# Malý princ

## Shrnutí děje

Pilot ztroskotá na poušti, potká Malého prince. Malý princ po pilotovi chce, aby nakreslil beránka - pilot nakreslí několik beránků, to se Malému princi nelíbí - nakreslí bedničku, ve které je beránek schovaný. Povídají si, Malý princ z malé planety B612, má tam růži, kterou má rád. Ale má pocit, že ona jeho ne, a tak odejde. 

Navštíví několik planet: planetu Krále, Domýšlivce, Pijana, Byznysmena, Lampáře, Zeměpisce a pak Zemi.

Přistál na poušti, kde byl jenom had. **Had** umí poslat všechny zpátky na jejich planetu uštknutím. Říká, že osamělí jsme nejen na pusté poušti, ale i mezi lidmi. Mluví v hádankách a tvrdí, že je všechny rozluští.

Pak potká na poušti **obyčejnou květinu**. Ptá se na lidi, ona viděla jen jednu karavanu a tak tvrdí, že lidí je 6 nebo 7. Vadí jí, že lidé nemají kořeny. 

Pak vystoupá na horu a zakřičí dobrý den, slyší jen ozvěnu. 

Pak viděl **zahradu plnou růží**. Uvědomil si, že jeho Růže není jedinečná a že jeho planeta je taky malá a obyčejná a že to zněj nedělá moc velikého prince a plakal.

**Liška** - může být pro Lišku jen obyčejný chlapec a ona pro něj jen obyčejná liška, ale když si ji ochočí, budou pro sebe jedineční

**Výhybkář** - přehazuje výhybky. Dospělí ve vlaku spí, děti koukají okolo. Jen děti vědí, co hledají - dospělí nejedou za ničím.

**Obchodník** - prodává pilulky, po kterých lidé celý týden nemusí pít. K čemu to je? Ušetří za týden 53 minut.

Je to rok od toho, kdy Malý princ dopadl na Zemi, a tak se vrací zpátky na poušť. Tam právě potkal pilota. Vydávají se s pilotem pro vodu, najdou studnu, napijou se. Malý princ se domluví s hadem, ten ho uštkne a Malý princ se vrátí na svou planetu.

Na konci - vzpomínka - je tomu šest let, stále je smutný z toho, že ztratil přítele.

## Kompozice

- osobní vypravěč, ich-forma
- retrospektiva
  - rámcový příběh: pilot ztroskotá na poušti a potká (nebo halucinuje) Malého prince
- dělení do krátkých kapitol (pojmenované podle toho, koho Malý princ potká)
- na začátku věnování a na konci vzpomínka

## Postavy

- **Malý princ** – zvědavý malý kluk, nechápe dospělé
- **Pilot** – vypravěč, dětský pohled na svět, ale i zodpovědnost
- **Růže** – chce po Malém princi, aby se o ni staral, ale sama jeho péči neopětuje, proto si princ myslí, že ho nemá ráda, a tak odejde
- **Liška** – chce po princi, aby si ji ochočil, ukázala princi krásu přátelství

## Historický kontext

- meziválečné období
  - souboj demokracie a totality
  - společnost je morálně na dně
- nové vynálezy ovlivňují život obyvatel
- vznik nových oborů
- odklon od náboženství

- *Rolland*, *Hemingway*, *Steinbeck*, *Remarque*

### Antoine de Saint-Exupéry

- prozaik, letec, reportáže z cest
- šlechtická Francouzská rodina
- v tvorbě často motivy letectví, filosofické úvahy

- *Válečný pilot* – osobní zkušenosti s válečným letectvím
- *Citadela* – souhrn morálních a filosofických zásad, nedokončená
- *Malý princ* – psáno v době 2. světové války

## Intertexty

Krása dětskosti a naivity

- O myších a lidech (postava Lennieho) – Steinbeck

Hledání sebe sama

- Kdo chytá v žitě – Salinger
