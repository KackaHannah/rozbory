# Na západní frontě klid

## Shrnutí děje

Příběh vypráví osudy pruského studenta a vojáka Pavla Baumera a jeho přátel během 1.svět.války. Pavel Baumer a jeho spolužáci předčasně ukončují školní docházku na gymnáziu, dobrovolně se přihlašují k tvrdému výcviku. Šikana od velícího jménem Himmelstoss. Při příchodu na frontu se jich ujme zkušený Polák Katczinski a pomáhá jim v získání prvních zkušeností.

Krušný život v kasárnách a zákopech, brzy je zabit
Kemmerich (jeden z přátel z dětství). Vzpomínání na život před válkou, ostatní ztrácejí původní válečné nadšení. Pavel Baumer získává dvoutýdenní propustku a odjíždí domů (matka má rakovinu). Zjišťuje, že je zcela odtržen od normálního života, začíná mít psychické problémy a válku již bere jako samozřejmost.

Vrací se na frontu. Při zajišťování vesnice zraněn přítel Kropp i sám Pavel. Převoz do nemocnice. Kroppovi amputují nohu. Pavel jde zpět na frontu a znovu se zde setkává s přítelem Katczinským, který je postřelen. Pavel ho odnáší do nemocnice, ale K. po cestě umírá. Pavla to silně zasáhne. Na podzim r. 1918 získává znovu volno. Přemýšlí  o svém životě, ale znovu se vrací na frontu. Umírá posledním výstřelem války (symbolický konec).

## Kompozice

- chronologický děj s retrospektivními pasážemi – vzpomínky hlavního hrdiny
- přímý vypravěč – z pohledu Pavla

## Postavy

- **Pavel Baumer** – hlavní hrdina, naivní mladík, později naprosto zlomen válkou
- **Albert, Müller, Leer, Kemmerich** – také naivní přátelé hl. hrdiny, postupně všichni umírají
- **Katczinski** - zkušený polský voják, pomáhá skupině kolem Pavla
- **Himmelstoss** – kasárenský šikanátor, ve skutečnosti zbabělec

## Historický kontext

- výpověď o 1. světové válce
  - reportážní styl
  - naturalismus, realismus
- ztracená generace
- kniha vydána v 30. letech → popis války nepohodlný pro nastupující nacismus 

- *Hemingway*, *Steinbeck*, *Barbusse*, *Rolland*, *Apollinaire*, *Wells*

### E. M. Remarque

- vlastním jménem Erich Paul Remarque
- hned po ukončení školy narukoval do armády, kde byl několikrát raněn
- odpor k válce, antimilitarismus
  - označen za literárního zrádce
- obtíže začlenit se do společnosti po válce a zbaven německého občanství
  - střídá mnoho (mnohdy bizarních) zaměstnání

- *Cesta zpátky*, *Tři kamarádi*

## Intertexty

Válka

- Komu zvoní hrana – Hemingway
- Petr a Lucie – Rolland
- Oheň aneb deník bojového družstva – Barbusse
- Osudy dobrého vojáka Švejka – Hašek

Naturalistické výjevy

- Kuře melancholik – Šlejhar

