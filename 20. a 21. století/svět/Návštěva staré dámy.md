# Návštěva staré dámy

## Shrnutí děje

Městečko Güllen, 20. století

Miliardářka Claire Zachanassian, ona titulní „stará dáma“, přijíždí na návštěvu do zchudlého maloměsta Güllen. Tam kdysi vyrůstala, ještě jako Klárka Wäscherová. Obyvatelé městečka si od návštěvy slibují finanční podporu, Claire zase pomstu za dávnou křivdu: v mládí otěhotněla s Gülleňanem Alfrédem Illem, ten však otcovství popřel a za pomoci falešných svědků vyhrál následný soudní proces. Klára musela v hanbě, bez peněz a bez pomoci opustit domov, později ale díky sňatku s ropným magnátem a několika dalším manželstvím získala obrovský majetek.

Nyní již vysoce vážená „stará dáma“ učiní Güllenským nehoráznou nabídku: věnuje městu miliardu, jestliže Illa někdo z obyvatel zavraždí. Ti nejprve návrh jednoznačně odmítnou, postupně však začnou podezřele utrácet a dělat dluhy, jako by počítali s tím, že se jejich finanční situace brzo zlepší. Ill se nakonec, přemožený strachem a pocitem viny, svým sousedům vydá. Starosta informuje tisk, že paní Zachanassian poskytne městu miliardový příspěvek díky Alfrédu Illovi, jejímu příteli z mládí. Gülleňané vytvoří uličku a Ill jí projde. Když se dav rozestoupí, Ill leží na zemi mrtvý. „Infarkt“ a „samou radostí“, zaznívá v komentářích lékaře i novinářů. Claire nechá mrtvého uložit do rakve, kterou si přivezla, a starosta obdrží miliardový šek.

## Kompozice

- chronologická kompozice + krátké vzpomínkové pasáže
- 3 dějství

## Postavy

- **Klára Zachanasjanová** – zraněná Illem, krutá, sarkastická, dává důraz na symboly (přiveze si rakev, má svého černého pardála), nezničitelná (náhradní kusy těla)
- **Alfréd Ill** – původně ve městečku velmi oblíbený, svou ženu si vzal pro peníze, oprávněně paranoidní, nakonec přijme svůj osud
- **Obyvatelé městečka** – vidí jen peníze, nechtějí ale přiznat, že pro peníze zabíjí člověka, vymýšlí tak všelijaké zástěrky a kličky

## Historický kontext

- druhá polovina 20. století
- existencialismus, postmoderna
  - Nový román – snaha porušit tradiční románovou formu
- snaha vzepřít se starým konvencím

- *Beatníci*

- *Rozhněvaní mladí muži*

- *Albee* – prvky absurdního dramatu

### Dürrenmatt

- dramatik, trochu i malíř a spisovatel
- inspirován epickým divadlem

- prvky absurdního dramatu

- epické divadlo
  - *Bertold Brecht*
  - herci svou postavu přímo nehrají, spíš o ní jen referují
  - cílem je donutit diváka porovnávat reálný svět s divadlem
- modelové drama
  - krajní situací se snaží upozornit na určitý jev ve společnosti

- *Fyzikové*

## Intertexty

Pomsta

- **Médea**
- Krysař – Vojtěch Dyk
- Žert – Kundera
- Frankenstein – Mary Shelley
- Hamlet – Shakespeare
- Hrabě Monte Cristo – Dummas

Peníze a moc

- Lakomec – Molliére

Maloměsto

- Zbabělci – Škvorecký
- Žert
