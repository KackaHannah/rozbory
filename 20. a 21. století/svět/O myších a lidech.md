# O myších a lidech

## Shrnutí děje

Kalifornie ve 30. letech 20. století

Hlavními postavami jsou Lennie Small a George Milton. Lennie není z nejchytřejších, je spíš uzavřený do sebe a po smrti tety Kláry se ho ujal kamarád George a chodí spolu pracovat. Lennie je na Georgovi závislý a má ho rád. George často Lenniemu vyčítá, že nebýt jeho, mohl si užívat, ale svým způsobem ho má také rád a nedal by na něj dopustit. I když Lennie moc rozumu nepochytil a vše hned zapomene, práce se neštítí.

Dostali práci na ranči v Soledadu, kde se seznamují s ostatními pracovníky. Oba sní o tom, jak si spolu pořídí své hospodářství a budou si pracovat na svém. George měl vyhlídnutý takový malý domeček, kde budou mít králíky a pole s vojtěškou. Během pobytu na ranči v Soledadu se k nim přidal ještě starý uklízeč Candy, který sice moc práce nezastane, ale pomůže jim finančně.

Curley, syn majitele ranče, má ženu, která s ním ovšem není moc šťastná, a tak často vyhledává společnost jiných mužů na ranči. Jednoho dne navštívila i Lennieho, kterému se líbily její vlasy, a tak si je chtěl pohladit, jenomže je hladil tak silně, že žena začala křičet. Lennie se vyděsil a nechtěně jí zlomil vaz. Utíkal se schovat na místo, které mu poradil George v případě, že něco provede. Když Curley zjistil, že je jeho žena mrtvá, vydal se hledat Lennieho a chtěl ho zabít. George však našel Lennieho dřív, a tak ho zastřelil on sám, aby ho ochránil před bolestí a krutým zacházením.

## Kompozice

- er-forma
- chronologické vyprávění
- děj končí na místě, kde i začínal
- členěno na kapitoly
- nijak spešl kompozice ani jazyk, velmi jednoduché

## Postavy

- **George Milton** – obětavý, stará se o Lennieho (pro ostatní je to nepochopitelné), bez Lennieho by se pravděpodobně stal dalším otrokem kapitalismu
- **Lennie Small** – mentálně postižený dobrák, velmi silný, miluje hebké věci (hladí je a omylem je při tom zabije)
- **Curley** – žárlivý zakomplexovaný malý muž, snaží si vynutit autoritu silou
- **Paní Curleyová** – hezká žena, nemiluje svého manžela, cítí se být osamělá a bez lásky, a proto často flirtuje s dělníky
- **Slim** – oproti Curleymu je klidný a autoritu má přirozeně
- **Candy** – starší mrzák, cítí se být nepotřebný, pomůže Georgeovi a Lenniemu výměnou za budoucí životní jistoty
- **Crooks** – otrok tmavé barvy, má velkou zálibu v literatuře

## Historický kontext

- meziválečná literatura, americký realismus
- krize amerického snu
  - pocity zklamání a skepse
  - rozklad hodnot
- krach na newyorské burze → Velká hospodářská krize (Great depression)

- *Faulkner*, *Hemminway*, *Fitzgerald*

### Steinbeck

- podobně jako Hemingway velmi vázaný na přírodu
- nepříznivé finanční poměry v rodině → už jako školák vypomáhá na farmách
- nejdřív studuje historii a literaturu, pak ale pracuje jako pomocný zedník, námořník apod.

- negativní postoj člověka k moderní civilizaci
- inspirace v outsiderech, zlodějích a tulácích
- typický hrdina je apatický a vyloučený ze společnosti

- *Hrozny hněvu*, *Na východ od ráje*

## Intertexty

Krása v naivitě

- Krysař (postava Seppa Jörgena) – Vojtěch Dyk

Vyřazení ze společnosti

- Frankenstein (postava Lennieho) – Mary Shelley

Krize tradičních povolání

- Stařec a moře – Hemmingway

Americká jižanská společnost (černí otroci)

- Dobrodružství Huckleberryho Finna – Mark Twain
- Dobrodružství Toma Sawyera – Mark Twain
