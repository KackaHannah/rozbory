# Proměna

## Shrnutí děje

Řehoř Samsa, obchodní cestující, se jednoho dne probudil a zjistil, že se proměnil v brouka. Chtěl vyrazit na pracovní cestu, členové rodiny na něj klepali, ale nemohl vstát. Do bytu přišel Samsův nadřízený, prokurista, a chtěl, aby šel Řehoř do práce. Ten odpovídá skrz zamčené dveře, že hned dorazí. Podaří se mu odemknout dveře a je šokovaný reakcemi ostatních. Matka pláče, prokurista uteče a otec ho novinami a holí zahnal zpět do pokoje.

Jediný z rodiny, kdo se Řehoře nestraní, je jeho sestra Markétka, pravidelně mu nosí jídlo. Řehoř uvažuje nad finanční situací rodiny, dosud ji totiž živil on. Chtěl sestře platit konzervatoř, ale nyní nemůže. Matka k Řehořovi do pokoje zpočátku vůbec nechodila, odhodlala se asi po dvou měsících. Řehoř ji nechtěl trápit, a tak zůstával skrytý pod pohovkou. Markétka a matka chtěly Řehořovi z pokoje odstranit nábytek, který nepotřeboval, a při této činnosti matka Řehoře zahlédla. Řehoř vyběhl z pokoje, aby nebyl matce na očích, ale to ženy vyděsilo. Když přišel otec z práce, snažil se Řehoře vyhnat zpátky do pokoje, honil ho po bytě a házel po něm jablka, z nichž jedno zůstalo zaryté v Řehořově těle. Sestra se snažila Řehoře uchránit.

Řehoř se léčil více než měsíc. Rodina mu nyní každý večer otevírala dveře a on pozoroval, co večer matka, otec a sestra dělají. Rodina finančně strádala, otec se nechal zaměstnat jako sluha, sestra jako prodavačka, ale stejně museli rozprodávat majetek. Markétka už o Řehoře tolik nepečovala – dvakrát denně mu nosila jídlo, ale už se nestarala, kolik toho sní. Pokoj byl špinavý a neuklidila ho ani nová posluhovačka. Ta jediná se Řehoře nebála, dokonce se na něj chodila dívat. V bytě nyní byli tři podnájemníci, kterým matka s Markétkou podstrojovaly. Jednou nájemníci seděli v obývacím pokoji, Markétka jim hrála na housle a oni zahlédli dveřmi Řehoře. Okamžitě dali výpověď. Markétka nyní poprvé vysloví myšlenku, kterou se všichni báli říct – Řehoř je jim na obtíž a je potřeba se ho zbavit. Řehoř všechno vyslechl a uznal, že jeho život nemá cenu. V noci vydechl naposledy, ráno ho našla posluhovačka. Celé rodině se ulevilo, mohli se nyní přestěhovat do menšího bytu.

## Kompozice

- 3 části
- vyprávěno chronologicky
- er- forma, vševědoucí vypravěč

## Postavy

- **Řehoř** – tvrdě pracující obchodník, nechává sebou trochu vytírat podlahu, živí celou rodinu, jakmile toho není schopen, je všem akorát na obtíž a těžce to nese
- **Markéta** – sestra Řehoře, citlivá, snaží se ho ze začátku zastávat, v knize je zmíněno jméno
- **Matka** – nedokáže se s proměnou Řehoře vyrovnat, spíše se ho bojí, více pasivní postava
- **Otec** – aktivně bují proti Řehořovi, nelíbí se mu, že musí pracovat (bídák jeden líná)

## Historický kontext

- meziválečné období
- poválečná krize a hyperinflace
  - narůstající moc nacismu → antisemitismus

### Franz Kafka

- otec židovský velkoobchodník
- studium práv – seznámení se s Maxem Brodem
- nemocen tuberkulózou
  - Max Brod po smrti vydává jeho dílo, i když si to Kafka nepřál
  - (Proměna vydána s vědomím Kafky)

- Pražský kruh
  - německy píšící autoři v Praze (často židovského původu)
  - *Meyer*, *Rilke*
- často se na dlouhé dny zabarikádoval do pracovny a nevylézal ven, dokud dílo nedopsal
- magický realismus, absurdní svět
  - kritika byrokracie
  - postavy často nemají jména


- *Proces*, *Zámek (nedokončeno)*

## Intertexty

Absurdita / Magický realismus

- Čekání na Godota – Beckett
- Mistr a Markétka – Bulgakov

Existencialismus

- Sartre, Camus
