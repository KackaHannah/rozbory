# Stařec a moře

## Shrnutí děje

Kniha vypráví o starém rybáři Santiagovi, pro kterého je rybaření jedinou obživou. Starci se ovšem dlouho nedaří chytit větší rybu, a proto je ostatním rybářům k smíchu. Aby toho nebylo málo, tak s ním na moře, kvůli zákazu svého otce, přestane jezdit velký pomocník, chlapec Manolin.

Stařec má ovšem svoji hrdost a rozhodne se všem dokázat, že stále dokáže chytit velkou rybu. Druhý den ráno se proto na moři vydá dál, než kdy byl. Kvůli svým zkušenostem ví, že pokud chce chytit velkou rybu, tak musí udice spustit do velkých hloubek.

Za nějakou dobu zabere opravdu velká ryba. Jenomže má spoustu sil a stařec ji proto nemůže ulovit. Zápasí s ní dva dny bez spaní a s drobnými zraněními. Rybář je ovšem rozhodnut, že nepovolí a rybu zabije, nebo že zabije ryba jeho. Po dvou probdělých nocích ryba ztrácí sílu a Santiago ji uloví. Zjistí, že je to mečoun a je několikrát větší než loďka.

Stařec ulovenou rybu přiváže k loďce a vydá se domů. Jenže krvácející ryba přivábí několik žraloků. Rybář se s nimi snaží bojovat, ale nemá šanci a žraloci mu rybu do posledního drobečku sežerou. Zůstane jenom kostra, se kterou se v noci vrátí do přístavu a vyčerpán odejde do svého domu. Ráno ho v posteli objeví chlapec, počká, až se probudí, a potom mu povídá, kolik lidí se shromáždilo kolem obří kostry. Lidé kostru ryby obdivují a on na sebe může být hrdý.

## Kompozice

- chronologické
- bez kapitol, členěno pouze odstavci
- střídání er- a ich- formy
  - vypravěč $\times$ rozmluvy Santiaga s chlapcem, rybou a sebou samým

## Postavy

- **Santiago** – starý konzervativní rybář, velmi skromný, pracovitý, pevné vůle, úcta k přírodě a moři (mluví k moři v ženském rodě)
- **Manolin** – obětavý chlapec, který věří v Santiaga a obdivuje ho, cílevědomý

## Historický kontext

- americká meziválečná próza
- rozpad amerického snu
  - sociální nejistota, nezaměstnanost, krize
- realistní až naturalistické romány
- rozvoj techniky → boj tradičních povolání s modernizací

- *Fitzgerald*, *Steinbeck*, *Faulkner*

### Hemingway

- ztracená generace
  - *Remarque*, *Fitzgerald*, (*Steinbeck*)
  - válečné zmrzačení
  - pacifistický náhled na svět, znechucení válkou
- jeho hrdinové jsou většinou v mezní životní situaci
- zkušenosti jako reportér v španělské občanské válce → dílo *Komu zvoní hrana*

- úspornost, strohost, úsečnost
- silná psychologizace postav
- metoda ledovce – pod hladinou je $\frac{7}{8}$ z celku

- *Sbohem armádo*, *Komu zvoní hrana*

## Intertexty

(Ne)přizpůsobování se změnám

- Višňový sad – Čechov

Generační rozdílnost/konflikt

- Žert – Kundera

Křesťanská/mýtická symbolika

- Utrpení Ježíše Krista (autor přirovnává utrpení rybáře na lodi k situaci, kdy rukama Ježíše projedou hřebíky)
- Mýtus o Sysifovi (nesplnitelný úkol)

Baseballista DiMaggio (stařec má jeho kartičku v chatrči)
