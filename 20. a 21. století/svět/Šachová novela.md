# Šachová novela

## Shrnutí děje

Děj díla Šachová novela se odehrává na velké zámořské lodi, která pluje z amerického New Yorku do argentinského Buenos Aires. Jedním z cestujících je i Mirko Čentović, jugoslávský mistr světa v šachu. Tato skutečnost samozřejmě nemůže zůstat utajena, a brzy se tak na lodi utvoří skupina amatérských šachových nadšenců, kteří vyzvou tohoto geniálního hráče na souboj. Čentović v souboji vidí příjemné zpestření dlouhé plavby, a tak souhlasí. První hra je pro jeho vyzyvatele velkým ponaučením, hraje se na jedné šachovnici, kdy na jedné straně je mistr světa a proti němu ostatní hráči, kteří hrají ve skupině. První partii Čentović s převahou vyhraje, jeho vyzyvatelé se ale nechtějí tak lehce vzdát a chtějí ještě jednu odvetnou hru, Čentović tedy souhlasí.

Zpočátku to vypadá, že opět snadno vyhraje, ale pak se stane něco nečekaného, když se ke skupině prohrávajících amatérů připojí neznámý muž. Ten jim poradí několik tahů, a jeho zásluhou tak partie skončí nerozhodně. Neznámý muž zvaný doktor B. není šachovým profesionálem, šachy se naučil hrát díky svému pohnutému životnímu osudu, o kterém ale nikdo neví. Všichni pak chtějí, aby další partii odehrál s Čentovićem on sám.

Vypravěč tedy vyhledává doktora B., který mu vypráví svůj příběh. Před nástupem Hitlera k moci se se svým otcem staral o zabezpečení církevního majetku před Nacisty. Kvůli tomu byl pak zatčen a rok izolován od okolního světa, systematicky mučen a vyslýchán. Od naprostého zlomení mu pomohla krádež knihy se záznamy historických šachových partií. Naučil se tedy nazpaměť všechny partie a nakonec začal hrát šachy jen sám se sebou. To ho dovedlo k naprostému šílenství, které ho pak dostalo i ven z vazby. Následně byl nucen opustit Rakousko, a tak se ocitá na této lodi.

V první partii doktor B. donutil svými tahy mistra světa k tomu, aby se vzdal. Ve druhé hře Čentović úmyslně prodlužoval své tahy, neboť si všiml, že doktora tato pomalost dráždí a začíná projevovat známky nepříčetnosti. Tato partie nakonec dopadla tak, že se díky svému stavu doktor B. při hře spletl a Čentović vyhrál.

## Kompozice

- chronologická s obsáhlými retrospektivními pasážemi
- osobní vypravěč
  - vypravěčem retrospektivních pasáží je doktor B.
- spisovný jazyk, šachové termíny

## Postavy

- **Doktor B.** – tajemný muž z rakouské šlechtické rodiny, milý a diskrétní, od zešílení z mučení nacisty ho zachránily šachy, z hry proti sobě nakonec také téměř zešílí
- **Mirko Čentović** – mistr světa v šachu, chladný, namyšlený, ve skutečnosti v každém jiném ohledu naprosto neschopný a hloupý, popisován jako stroj, alegorická kritika fašismu

## Historický kontext

- Nenaplněné ambice poražených států
  - podhoubí pro vznik totalitních režimů – nacismus, fašismus

- Německo prohrává 1. světovou válku
  - je nuceno platit absurdně vysoké reparace → hyperinflace
  - 1923 pivní puč – první (nevydařené) snahy nacistů dostat se k moci
  - 1933 je Hitler dosazen do funkce říšského kancléře
  - 1935 vydány Norimberské rasové zákony
  - 1938 anšlus Rakouska, Mnichovská dohoda
  - 1939–45 druhá světová válka

- *Franz Kafka*
  - židovský autor německého původu působící v Praze
  - *Proces*, *Zámek*, *Proměna*
- *Karel Čapek*
  - velmi blízký s T. G. M., pátečník, předseda PEN klubu
  - Mnichovská dohoda pro něj znamenala prakticky zhroucení světa
  - jeho bratr, Josef, umírá v koncentračním táboře
- *Karel Poláček*, *Eduard Bass*

### Stefan Zweig

- spisovatel, autor životopisných románů a překladatel
- židovský původ
- odpůrce války a násilí
- přátelství se Sigmundem Freudem
  - velmi ovlivněn psychoanalýzou a civilismem

- po anšlusu Rakouska emigruje, aktivně vystupuje proti fašismu
  - v roce 1942 vydává Šachovou novelu jako varování před fašismem, následně páchá sebevraždu


## Intertexty

Šachy

- Zahradní slavnost (postava Huga) – Václav Havel

Varování před fašismem/nacismem

- Bílá nemoc – Karel Čapek
- Válka s mloky – Karel Čapek
- Caesar (neschopný vládce, přirovnávání k Mussolinimu) – Voskovec a Werich

Kritika totality

- 1984 – Orwell
- Farma zvířat – Orwell
