# Blues pro bláznivou holku

## Shrnutí děje

- **Prolog**
  - věnuje báseň obyčejným lidem, příbuzným, dělníkům, …

- **Variace na renesanční téma**
  - nejznámější text Václava Hraběte. O její popularizaci se v sedmdesátých letech zaslouží zejména Vladimír Mišík, o několik let později pak písničkář Vladimír Veit. Zmiňovanou variací je v tomto případě moderní obdoba středověkých svítáníček, milostných písní o ranním loučení milenců. Variace toho jaká je láska. Na první pohled se jedná o jednoduchou písňovou formu o čtyřech strofách, pravidelně rýmovanou, jen v úvodní strofě je rytmus báseň tematicky narušen.
  - Inspirace Villonem
- **Blues na památku Vladimíra Majakovského**
  - skladba, tempo, rytmus, téma básně na hudebním vystoupení, Van Gogh, podobný osud jako Majakovský
  - Majakovskij – zakladatel ruského futurismu, odpor proti carskému režimu, sebevražda
- **Báseň skoro na rozloučenou**
  - vrací se zpět k Prologu a reaguje na něj, cítíme, jak se lyrický subjekt ztotožňuje s těmi, které v Prologu opěvoval.

## Kompozice

- sbírka básní
  - 12 básní, na začátku Prolog, na konci Báseň skoro na rozloučenou
- volný verš, hudebnost
- absence interpunkce
- neotřelá přirovnání, nápaditost, originalita
- typický časoprostor – večerní Praha

## Postavy

- **Lyrický subjekt** – zamilovaný, melancholický, záměrně se stylizuje do role outsidera či prokletého básníka, někdy píše z pohledu ženy

## Historický kontext

- 1940–1665
- **Beat generation**
  - 50. až počínající 60. léta
  - umělecké hnutí mladých lidí v USA
  - konzum, materialismus, morální pokrytectví $\times$ snaha žít jinak
  - inspirace v anarchismu, nihilismu
  - alkoholismus, užívání drog

- Jack Kerouac
  - *Na cestě* – dílo s autobiografickými prvky, spontánní próza
- Allen Ginsberg
  - *Kvílení* – plné aluzí a intertextů, zoufalé a mezní situace autorových vrstevníků

### Václav Hrabě

- jazzový hudebník, učitel, dělník i redaktor
  - odmítá umístěnku na pedagogické místo v Kraslicích → obtíže se sehnáním bydlení
- láska k Praze
  - typický časoprostor jeho děl (nejčastěji ještě večer)

- "největší český Beatník"
  - osobní setkání s Allenem Ginsbergem
- **inspirace Villonem** – Variace na renesanční téma
- posmrtná popularita a zhudebňování

## Intertexty

Asociativní poezie

- Alkoholy – Apollinaire
- Edison – Vítězslav Nezval

Básně o lásce

- Sonety – Petrarca
