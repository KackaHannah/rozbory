# Báječná léta pod psa

## Shrnutí děje

V roce 1968 se Kvido se svými rodiči přestěhuje po ruské okupaci z Prahy do Sázavy. Kvidův otec nebyl členem komunistické strany, a proto nemohli dostat byt. Komunista Šperk ale slyší, jak Kvido ve škole recituje komunistické básně, a tak se rozhodne i přes nezaujatost jeho rodičů dát jeho otci byt. Kvidův otec je ve svém zaměstnání dobrý, postupuje v něm dále, časem může dokonce i jako jeden z mála nekomunistů jezdit na zahraniční pracovní cesty. Vše se pokazí, když se setká se spisovatelem Kohoutem, jenž je odpůrce režimu. Přijde tak o dobrou práci, začne být melancholický a má deprese. Kvido přestane studovat na vysoké škole, aby se mohl více věnovat psaní povídek. Stane se z něj vrátný. Mezitím si jeho otec pod tíhou psychických potíží dělá rakev. Kvidova matka syna prosí, aby otce nějak rozveselil a měl se svou přítelkyní Jaruškou dítě. Psychická nemoc však přetrvává nadále i po narození vnoučete. Kvido píše román, který ale nemůže být vydán kvůli režimu. Po roce 1989 se vše zpraví. Kvidův otec se uzdraví a román je vydán pod názvem Báječná léta pod psa.

## Kompozice

- Retrospektiva
- některé části psány deníkovou formou, nebo jako divadelní scénář
- autorský vypravěč – er-forma, Kvido události komentuje

## Postavy

- **Kvido** – vypravěč, neschopný intelektuál (už jako malé dítě), záliba v jazyce a psaní, autorovo alter ego

- **Kvidův otec** – pochází z chudších poměrů, neprůbojný, má problémy s režimem (nakonec ze z něj téměř zblázní)
- **Kvidova matka** – ochotná a milující, neúspěšná herečka

## Historický kontext

- 60. léta
  - hospodářská krize
  - Pražské jaro, socialismus s lidskou tváří → vpád vojsk Varšavské smlouvy
- 70. a 80. léta
  - normalizace
  - další politické procesy
  - Charta 77 → vznik Anticharty (která spíš neúmyslně dělala Chartě 77 promo)
  - lidová apatie k politické situaci
- Sametová revoluce
  - 1985 – Michail Gorbačov prosazuje Perestrojku
  - růst občanské iniciativy – Několik vět", OF
  - nakonec vytvoření nové vlády, KSČ jen menšinové zastoupení

- *Pavel Kohout*
  - vystupuje v románu
  - básník, prozaik, dramatik, významný spisovatel samizdatu v exilu
  - nejdříve budovatelská poezie, poté z komunistické ideje vystřízlivěl
  - výrazná osobnost pražského jara a Charty 77 
- *Jaroslav Seifert*, *Václav Hrabě*, *Václav Havel*, …

### Michal Viewegh

- prozaik, fejetonista, publicista
- nejdříve hravý a postmoderní, poté povrchní a podbízivý
  - nejprodávanější český spisovatel


- Postsocialistická nostalgie
  - vzpomínání na socialismus jako na ty "dobré časy"
  - boj s režimem nebo disent většinou zmíněn jen velmi okrajově
  - často očima dítěte
  - autobiografické prvky

- *Báječná léta pod psa*, *Účastníci zájezdu*

## Intertexty

Totalita očima dítěte

- Bylo nás pět – Karel Poláček

Komunistický režim

- Žert – Milan Kundera
- Mistr a Markétka – Michail Bulgakov
