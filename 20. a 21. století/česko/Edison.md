# Edison

## Shrnutí děje

noční Praha, 19. století

1. Sebevražedný stín, který provází básníka. Je to jeho kontrast a zároveň i jeho součást.
2. Lyrický subjekt najde v novinách článek o úspěšném vynálezci Edisonovi. Vzpomíná na své dětství, kdy roznášel noviny.
3. Popis Edisonových vynálezů a oslava pokroku civilizace.
4. Smyslem života je štěstí z poznání, nových vynálezů a objevů.
5. Radost ze života, lyrický subjekt se probouzí ze snění a uvědomuje si, že rozhovor vedl celou dobu sám se sebou.

## Kompozice

- 5 zpěvů

  - dlouhé sloky
  - střídání naděje a pesimismu, hra s kontrastem
  - báseň končí tam, kde začala

- volný verš, sdružený rým

- refrén

  - "Bylo tu však něco těžkého co drtí

    smutek stesk a úzkost z života i smrti"

  - "Bylo tu však něco krásného co drtí

    odvaha a radost z života i smrti"

  - časté anafory a epifory

## Postavy

- **Lyrický subjekt** – pokládá se v kontrastu k Edisonovi jakožto sebevrah, neúspěšný hazardér s prázdným životem

## Historický kontext

- poetismus
  - 20. léta
  - styl jedinečný pro Česko
  - společenské vystřízlivění ze vzniku Československa a vyčerpání proletářského umění
  - inspirace Apollinairem a Marinettim
  - umění žít a prožívat štěstí, bezstarostná skutečnost
  - básníkem a umělcem může být každý
  - témata a motivy: moderní svět, jazz, film, fotbal, cestování
  - ve 30. letech více obav, možnost krize → rozpad :(
- fascinace vědeckým pokrokem, změna životního stylu
- *Devětsil*, *Taige*, *Nezval*, *Seifert*, *Vančura*

### Vítězslav Nezval

- levicově smýšlející $\times$ odmítá proletářské umění
  - po 2. sv. válce pak ale píše budovatelskou poezii
- přední meziválečný avantgardista a překladatel
- člen skupiny *Devětsil*
  - umělecký svaz
  - komunistické myšlenky, očekávání revoluce
  - *Taige*, *Seifert*, *Wolker*, *Vančura*, *Toyen*, *Hoffmeister*, *Voskovec*, …
- surrealismus, poetismus

- *Manon Lescaut*
  - veršované drama podle románu Abbé Prévosta
- *Edison*
  - fascinace technikou, pokrokem a vynálezy
  - co člověk za sebou zanechá po své smrti

## Intertexty

Slavní vynálezci

- **Edison**, Tesla, Newton, …
