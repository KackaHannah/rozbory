# Hordubal

## Shrnutí děje

Podkarpatská Rus, vesnice Krivá

Hlavní postava Juraj Hordubal se po osmi letech vrací z Ameriky do vesnice Krivá za svou ženou a dcerkou. Po cestě vzpomíná na těžkou práci v dolech a přemýšlí, jak by Polanu překvapil. Myslí také na to, jak si za našetřené peníze koupí krávy a bude s nimi chodit po kopcích, zatímco se budou pást. Ale jakmile se objeví před domem, Polana zůstane stát jako opařená a nepadá mu kolem krku, jak doufal. Dcerka před ním utíká a čeledín Manya je jí bližší. Zároveň zjišťuje, že jeho louky jsou prodány a Polana nakoupila nížiny, aby Manya mohl pást koně, o které Hordubal nestojí.

V hospodě ho nepoznávají a on se tam dozví, že mu byla Polana nevěrná. Nechce tomu věřit a porve se se svými bývalými kamarády. Aby ukázal, že tomu tak není, zasnoubí Hafii s Manyou. Lidé se začnou Manyovi vysmívat a ten si stěžuje u Hordubala. Ten se rozzuří, roztrhá smlouvu a vyžene Manyu. Náhodou se na vlastní oči přesvědčí, že Polana je mu stále nevěrná a začne mu být všechno jedno. Nedbá o své zdraví, když běhá za pasákem ovcí Míšou daleko do hor, kterému se svěřuje. Nakonec ho najdou doma ve chlévě, jak leží s horečkou. Uloží ho do pokoje a Polana se o něho – alespoň trochu – stará. Ráno ho však naleznou mrtvého.

Přijde starosta, četníci, lékař, který zjistí, že Hordubal, jenž ostatně už byl na smrt nemocen, byl zabit tenkým předmětem přímo do srdce. Jeden z četníků se snaží, aby to vypadalo, že Hordubal zemřel na zápal plic, ale druhý z nich chce za každou cenu objasnit celou tragédii. Je zatčena Polana i Manya, ačkoliv oba obvinění popírají, tak jsou nalezeny u Manyy doma Hordubalovy peníze a nakonec i jehla, která by mohla způsobit ránu v Hordubalovu srdci. K dalšímu přelíčení se dostaví celá vesnice, aby mohla odsouhlasit ne zločin, ale hřích. Štěpán se přizná a je odsouzen na doživotí. Polanu odsoudí k trestu za spoluvinu.

Srdce Juraje Hordubala se někde ztratilo a nebylo nikdy pohřbeno.

## Kompozice

- Reflektovaný vypravěč
  - časté monology a polopřímá řeč
  - nespolehlivý

## Postavy

**Juraj Hordubal** – velmi pracovitý hodný člověk, naivní, miluje Polanu a vidí ji jen v tom nejlepším světle

**Polana** – osobnostně nevýrazná, rozkol mezi Hordubalem a Štěpánem, 

**Štěpán Manya** – milenec Polany, má rád Hafii jako vlastní dceru, dobře se o ně stará (Hordubalovi tak nepřipadá), rád využívá Hordubalových peněz

## Historický kontext

### Karel Čapek

- 1890-1938

- symbol Československa

- opakovaná nominace na N. cenu, dobový mezinárodní úspěch, předseda českého PEN klubu

- novinář, překladatel, povídkář, romanopisec, dramatik, esejista, kritik a autor knih pro děti

- velmi blízko k Masarykovi – **Pátečníci**

- Bechtěrevova choroba (chronické záněty obratlů)


- Čapkův recipient

  1. nejdříve cílí na náročnější čtenáře

  2. - filosofické úvahy, modernismus
     - *Stín kapradiny*

  3. poté cílí na lidového a obyčejného člověk

  4. - dostává hroznou bídu od rodiny
     - popularita? budování státu?
     - *Povídky z jedné a druhé kapsy*


- noetická trilogie

  - *Hordubal*, *Povětroň*, *Obyčejný život*
  - nemožnost poznat pravdu
  - různící se pohledy policistů na zločin, Hordubala a ostatních obyvatel vesnice na vztah Polany a Štěpána

## Intertexty

Obrácení společnosti proti jedinci

- Proměna – Franz Kafka

Podkarpatská Rus

- Golet v údolí – Ivan Olbracht
- Nikola Šuhaj loupežník – Ivan Olbracht

Pochopitelná nevěra

- Paní Bovary – Gustav Flaubert
