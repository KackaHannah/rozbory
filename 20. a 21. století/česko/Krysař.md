# Krysař

## Shrnutí děje

Německé městečko Hammeln, období hansovních měst (12.-17. století)

Krysař přijde do Hammeln, potká Agnes a zamilují se. Konšelé mu nabídnou dost peněz za to, že vyžene z města krysy. On je vyžene, ale zaplaceno nedostane - smlouva prý neplatná, nabídnou mu, že mu zaplatí ve výrobcích. Pro město to totiž byla přemrštěná cena za nevelkou námahu Krysaře.Krysař ví, že svou píšťalou může celé město zničit, ale ušetří ho pro Agnes. Krysař se setká s Dlouhým Kristiánem (Krysař na zahradě Agnes, Kristián venku - je to jasné). Jde se projít, pak se vrátí do města a jde do hospody, kde se setká s magistrem Faustem, který prodal duši ďáblu a láká Krysaře, aby to taky udělal. Faust si myslí, že umí kouzlit díky ďáblovi, ale nic se neděje. Pak prostě zmizel. Agnes otěhotní s Kristiánem, ale nechce jeho dítě a spáchá sebevraždu skokem do propasti pod horou Koppel. Krysař zapíská na píšťalu, ovládne obyvatele města a všichni skočí do propasti. Pak tam hodí i svoji píšťalu a skočí tam taky. Jediný, kdo nereagoval na pískání píšťaly byl Sepp Jörgen, takže přežil a našel plačící mimino, které opustili rodiče kvůli zvuku píšťaly. Sepp Jörgen + dítě = nová, neposkvrněná naděje pro město / lidstvo.

## Kompozice

- chronologická výstavba
- 26 kapitol bez názvu
  - vložka: píseň o zemi **Sedmihradské**
- er-forma, osobní (?)
- spisovný jazyk, krátké věty

## Postavy

- **Krysař** – tajemný a chladný, lidé se ho štítí, i když ho potřebují, cestuje sám a bez závazků, je ochoten připoutat se k Agnes, neboť ji miluje, není jasně dobrý ani špatný
- **Agnes** – otevřená krásná dívka, ze začátku mladá a radostná, má milence Dlouhého Kristiána, ale zamiluje se do Krysaře
- **Dlouhý Kristián** – krásný mladík, vypočítavý, sobecký, žárlivý – znásilní Agnes a udělá jí dítě, aby se "zbavil konkurence" (ewww)
- **Sepp Jörgen** – docela pohledný, ale mentálně zaostalý rybář, ostrakizován, jako jediný uteče moci píšťaly (probere ho pláč dítěte)

## Historický kontext

- přelom 19. a 20. století
  - politická nestabilita, doba napětí a zklamání u nás
- moderna, česká dekadence, **buřiči**
  - skupina mladých autorů (nejčastěji básníci)
  - antimilitaristé nuceni narukovat do 1. světové války

- *Neumann*
  - vůdčí osobnost buřičského hnutí
  - zakládá časopis Nový Kult
  - vývoj tvorby: buřičství → proletářská poezie
- *Toman*
  - psal o tuláctví a svobodě
- *Prokletí básníci*
- *Březina*

### Vojtěch Dyk

- buřičství a symbolismus
  - odmítavá reakce na realismus
  - důraz na smysly a city, práce se čtenářovou představivostí

*Milenka sedmi loupežníků*

## Intertexty

Krysař

- středověká německá legenda

Zabití z pomsty

- Návštěva staré dámy – Dürrenmatt

Balada

- Kytice – Erben

Ďábel

- Faust – Goethe

Fantasy

- Zaklínač – Sapkowski
