# Píseň o Viktorce

## Shrnutí děje

Viktorka je krásná mladá dívka/žena, která se ráda baví a ráda tančí a mnoho mládenců po ní touží. Viktorka se zakouká do tamního myslivce, kterého se zároveň trochu bojí. S myslivcem stráví noc a on ji následně opustí. Viktorka všeho lituje, stydí se, obrací se k Bohu a následně i zešílí. Viktorka narozené dítě utopí, poté ji v bouřce zabije blesk.

## Kompozice

- poezie
  - obkročný rým, sylabotónický verš (??)
  - poslední 2 sloky verš volný a bez rýmů
  
- 7 částí
  - sloky o 4 verších
  - předposlední 4 verše, poslední 3 verše (a to i ve 4. spešl části)
  
  - 4. část speciální
    - střídá se narativ Viktorky, myslivce a externího vypravěče
    - každá sloka o 6 verších ABABBA
    - první verš sloky stejný jako poslední

## Postavy

- **Viktorka** – vykreslována velmi romanticky, něžně, ale i šíleně
- **Myslivec** – výrazné pronikavé oči, vykreslován spíš jako chladný a bezcitný

## Historický kontext

totalita

- vítězný únor 48
- politické procesy 50. let
- hospodářská krize 60. let
- pražské jaro
- okupace 68

### Jaroslav Seifert

- Nobelova cena za literaturu, značná popularita

- 20. léta – proletářské umění, poetismus (na vlnách TSF)
- 30. léta – vlastní poetika, vzpomínky na dětství a nostalgie
- 40. léta – vlastenectví i děkování osvoboditelům
- 50. léta – stále vlastní poetika, dostává za to trochu čočku
  - *Píseň o Viktorce*
    - velký intertext na *Babičku*
      - na rozdíl ode všech postavu Viktorky romantizuje a interpretuje jinak
    - paralela s životem Boženy Němcové
  - *Maminka*
    - nenávratnost dětství, melancholie dospělého
  - přednes na sjezdu spisovatelů v roce 1956 o "lhaní"

- 60. léta – zhoršený zdravotní stav, změna poetiky, proti okupaci v roce 68, signatura Charty 77
  - *Odlévání zvonů*
  - *Morový sloup*
  - *Všecky krásy světa*

## Intertexty

Viktorka

- Babička – Božena Němcová

Zabití dítěte

- Médea (narozdíl od Viktorky zabije děti z pomsty)
