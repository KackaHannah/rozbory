# Romeo, Julie a tma

## Shrnutí děje

Student Pavel má část maturity za sebou, za odměnu si koupí lístek do kina (má rád filmy), ale před filmem má ještě čas tak se jde projít po parku. Usedne na lavičku a později si všimne, že vedle něj sedí dívka a pláče. Za začátku jí nevěnuje pozornost a myslí si, že je nehezká. Později se s ní dává do řeči, dívka se mu svěřuje se svým osudem. Že je židovka, že její rodiče jsou v Terezíně (a ona věří, že stále naživu) a ona se měla dnes ráno dostavit na vlak do pracovního tábora.

Pavel ji nabízí úkryt v jeho místnosti (kterou mu věnoval otec, vedle jeho krejčovské dílny) a dívka se stěhuje do malého útulného pokojíku. Zde však musí dodržovat přísná pravidla (nesvítit v noci, nedělat hluk, neukazovat se u okna atd.) a Pavel ji sem chodí navštěvovat kdykoliv může. Nosí jí sem jídlo, knížky a snaží se, aby byla šťastná. Jídlo musí krást doma potají (nechce se rodičům svěřit, bojí se o ně, dochází k rozporu mezi nimi), anebo kupovat za peníze, které získává prodejem svých věcí, které měl dřív tak rád, ale teď mu přijdou zbytečné a malicherné oproti tajemství které musí střežit.

Ester a Pavel se do sebe zamilují, plánují společnou budoucnost, svěřují si své sny, vypráví si o své minulosti (především Ester vzpomíná na kamarádku Jitku, školu, kterou měla tak ráda, tancováni s tatínkem v obýváku, a první lásku Jarka). Venku mimo pokojík houstne atmosféra (atentát na Heydricha) Pavel však nechce Ester znepokojovat a proto kdykoliv se ho na to zeptá, vyhýbavě odpoví nebo Iže (za ukrývání židů hrozí všem přistiženým smrt, dlouhé seznamy popravených, i jednoho hudebníka z domu, ale Ester se to dovídá potají z radia v pokojíku). 

Jednou večer se jde do krejčovské dílny umýt, přistihne ji tam tovaryš Čepek a ten dává zprávu krejčímu (otec Pavla, a je z toho špatný, ale úzkostlivě to musí s Pavlem tajit před nemocnou matkou a domluví se na přestěhování Ester na venkov) Ester už to v malém pokojíku nemůže déle vydržet (po nocích, a když tam Pavel není, potichu brečí, ze strachu z budoucnosti, o Pavla atd.) proto Pavel slibuje, že ji odveze na venkov, kde bude v bezpečí. V noci před odjezdem se však v domě ozývají výstřely a opilý kolaborant Rejsek buší na dveře od pokoje a křičí na celý dům, že je tam židovka, později jde i dovnitř (pomáhá Čepek) aby ji neobjevil, vše vyjde, dívka se však nemůže smířit s tím, že by se kvůli ní někomu něco stalo. Proto vybíhá ven na ulici, utíká a potom klesne do trávy (byla zastřelena skoro okamžitě německými vojáky).

## Kompozice

## Postavy

- **Ester** – židovka, odvážná, ale stále brečí (cringe, nemam ji ráda), z lásky k Pavlovi se rozhodne obětovat
- **Pavel** – obětavý student, starostlivý, rozkol mezi Ester a rodiči
- **Pavlovi rodiče** – starostliví, obětaví, nerozumí změně chování v jejich synovi
- **Čepek** – tovaryš, pomáhá Pavlovi židovku utajit
- **Rejsek** – udavač

## Historický kontext

- Holocaust v literatuře
  - neetické $\times$ informování široké veřejnosti
- politizace literatury
  - radikální odklon od pravice
  - Sorela, budovatelské romány

- Arnošt Lustig
  - český židovský spisovatel
  - prošel koncentračními tábory
  - *Modlitba pro Kateřinu Horovi Horovitzovou*
- Josef Škvorecký – *Zbabělci*

### Jan Otčenášek

- prozaik, scénárista a spisovatel
- proti Nacismu, tvoří díla s budovatelskou tématikou
  - zabývá se válkou a holocaustem
- působení ve Svazu českých spisovatelů

- *Občan Brych* – román o únorovém převratu

## Intertexty

Láska

- **Romeo a Julie** – William Shakespeare
- *Petr a Lucie* – Romain Rolland

Holocaust

- *Sophiina Volba* – William Styron
- *Schindlerova archa* – T. Keneally
- *Maus* – Art Spiegelman
- *Modlitba pro Kateřinu Horovitzovou* – Arnošt Lustig
