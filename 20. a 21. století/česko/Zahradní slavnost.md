# Zahradní slavnost

## Shrnutí děje

Hugo je rodiči vyslán na zahradní slavnost Likvidačního úřadu, aby se setkal s vlivným Františkem Kalabisem. Hugo Františka nenajde, ale zapříčiní sled absurdních situací. Všichni hodnostáři mluví funkcionářským bezobsažným jazykem. Hugo se rychle naučí jejich styl komunikace jako univerzální dorozumívací techniku. Nakonec stane sám v čele nově ustavené Ústřední komise pro zahajování a likvidování. Tím následně ztratí vlastní identitu. Nakonec ho nepoznávají ani vlastní rodiče.

## Kompozice

- cykličnost
  - dějová i opakování gest a slov
- neosobní fráze, degenerovaný jazyk

## Postavy

- **Hugo Pludek** – dosahuje společenského postavení pomocí omílání prázdných frází, svět je pro něj šachová partie, postupný rozklad osobnosti vlivem společnosti a byrokracie
- **Otec** – občas prohodí naprosto náhodné a nesouvisející přísloví
- **Petr** – bratr Huga, černá ovce rodiny, rodiče se ho snaží zapřít, intelektuál, nesnaží se splynout s davem

## Historický kontext

- totalita
  - vítězný únor 48
  - politické procesy 50. let
  - hospodářská krize 60. let
  - pražské jaro
  - okupace 68
- absurdní drama
  - jazyk jako nástroj nedorozumění
- rozmach divadel malých forem
  - vynalézavé, alternativní, kabarety
  - *Suchý a Šlitr*
  - po roce 68 prakticky zakázány → bytová divadla

- *Seifert*, *Kundera*, *Hrabě*, *Hrabal*, *Klíma*

### Václav Havel

- vyrůstá ve velmi bohaté rodině → komunisty nepřijímáno
- práce jako kulisák, poté jako dramaturg, působení v časopise Květen
- česky disent
  - Občanské fórum
  - Charta 77
- po revoluci zvolen prezidentem

- absurdní divadlo, eseje
  - přizpůsobování, bezpáteřnost, oportunismus, jazyková vyprázdněnost
  - zpočátku se věnuje více satiře byrokracie a společnosti, poté se více soustředí na reflexi vlastního života

- *Audience*, *Moc bezmocných,* *Antikódy*

## Intertexty

Šachy

- Šachová novela (hraní sám se sebou) – Zweig

Byrokracie, mechanismus proti jedinci

- Proces – Kafka

Absurdní drama

- Čekání na Godota – Beckett
