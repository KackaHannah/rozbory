# Zbabělci

## Shrnutí děje

Kostelec, konec 2. světové války

Děj probíhá na konci druhé světové války v českém maloměstě během jednoho týdne na samém konci okupace (od 4. do 11. května 1945, ale příležitostně se vrací do minulosti). Na začátku zůstávalo dění za zájmy mladých lidí, kteří žijí džezem a poznávají své první lásky. Děj se točí kolem Dannyho, kterému je sotva 20 let a nejvíce ho přitahuje Irena. Ale ta má ráda mladého horolezce Zdeňka. Ráda sice přijímá Dannyho dvoření, ale Zdeňkovi zůstává věrná. Mladí mají dojem, že se proti okupantům nic neděje. Revoluci dělají ve městě představitelé městské smetánky podle svých představ. Ovšem spíš tak, že se všemožně snaží, aby se prakticky nic nedělo, a když už konečně mobilizují, tak jedině proto, aby vedli dobrovolníky ke zbytečnému hlídkování. Městský štáb se nakonec s Němci dohodne na jejich nerušeném odchodu k Američanům. 9. května se začíná jásat, protože se rozšíří zpráva, že se už blíží ,,Rusáci“, ale před nimi zcela nečekaně vjedou do města německé tanky. Dojde k boji, do kterého se vrhnou mladí s několika dělníky, a to proti vůli velitele, kterého nakonec z toho všeho trefí mrtvice. Danny se střetnutí účastní s Přemou, kterého velitel zajistil ve sklepě a kterému se podaří uniknout. Danny šťastně přečkal i krátké zajetí německými vojáky. Vysloužil si tak sice obdiv a políbení od Ireny, avšak pak zjistí, že ho ,,má jen ráda“ a že miluje Zdeňka.

## Kompozice

- osobní vypravěč
- podoba deníku
  - 8 kapitol – každá věnována jednomu dni

- chronologické vyprávění

## Postavy

- **Danny Smiřický** – lehkomyslný floutek, zajímají ho jen ženy, jazz a kamarádi, beznadějně zamilovaný do Ireny
- **Irena** – starostlivá a nápomocná, nemá o Dannyho zájem

## Historický kontext

- konec 2. světové války
  - pád Berlína, osvobozování českých zemí, květnové povstání
  - doznívající bitvy na venkově
- povstání Čechů proti německým okupantům

### Josef Škvorecký

- Danny Smiřický
  - Alter ego, objevuje se ve více románech
- tzv. román deziluze
- v Kanadě založil nakladatelství Sixty-Eight Publishers
  - vydávání exilových a zakázaných autorů
  - velmi úspěšné


- *Tankový prapor*, *Prima sezóna*

## Intertexty

II. světová válka

- Hlava 22 – Heller
- Romeo, Julie a tma – Otčenášek

Deziluze

- Žert – Kundera
- Sekyra (z postavy otce se postupně stává nepřítel) – Vaculík

Mladý hrdina ztracený v životě

- Kdo chytá v žitě – Salinger