# Švestka

## Shrnutí děje

Hra je uvozena následujícími referáty (v závorce je název postavy, kterou zpravidla příslušný přednášející ztvárňuje ve hře):

- Cimrmanův krokový defekt (Eliška Najbrtová, Jenny Suk, Anička Šafářová)
- Písně s dentální tematikou (Sváťa Pulec)
- Železniční stomatolog (Blažej Motyčka)
- Japonská inspirace (Eda Wasserfall)
- Dva stařecké neduhy (Přemysl Hájek)
- Hudební tečka – píseň „Šel nádražák na mlíčí“ (všichni účinkující)

Vlastní hra se odehrává v železniční staničce Středoplky, do níž přijíždí bývalý vechtr Přemysl Hájek, kterému patří strom švestky v zastávce. Má povolení ji očesat, ale musí to stihnout do večera. Jelikož mu mladý vechtr Kamil Patka odmítne půjčit žebřík, chce Přemysl využít zkušenosti svého kamaráda, horolezce Sváti Pulce. Ten ovšem přijede na invalidním vozíku a je neschopen lezení na strom. Také Přemyslův bratranec Blažej Motyčka je pozván, aby pomohl očesat a získal třetinu úrody. Zdánlivě lehký úkol je komplikován faktem, že Přemysl Hájek trpí stařeckým neduhem, totiž neschopností udržet myšlenku, a stále zapomíná, co se po něm chce. (Zároveň každé osobě na uvítanou podává ruku a odmítá ji pustit.) Naopak Blažej Motyčka je neschopen myšlenku opustit a neustále ostatní zdržuje vyprávěním postřehů k tématu, které bylo probráno a již se o něm nemluví. Další zdržení přivodí postupně tři ženy, které stanicí projdou – Emilka Najbrtová, Jenny Suk a Andulka Šafářová. U všech se mladý Kamil Patka pokouší získat přízeň, ale až poslední z nich je ochotna se za něj provdat. Náhodně příchozí Eda Wasserfall z Klubu českých turistů vytvářející turistické značení se snaží na švestku natřít značku, přičemž Sváťa se ho snaží od tohoto úkolu odradit, aby mohli švestku očesat, zatímco Přemyslovi jeho záměr nevadí. V závěru hry přijíždí drezínou železniční úředník Kryštof Nastoupil, kterému úroda propadla, protože Přemysl nestihl švestku očesat.

## Kompozice

- 2 části – seminář a divadlo
- jednoduchá chronologická
- postavy postupně přicházejí a otevírají tím nové scény
- ve výsledku se tam ale stejně nic neděje

## Postavy

- **Kamil Patka** – současný vechtr, žije sám ve vechtrovně, rád by se oženil

- **Přemysl Hájek** – bývalý vechtr, přehnaně srdečný, neschopen udržet myšlenku

- **Blažej Motyčka** – starý přítel Hájka, neschopen myšlenku pustit

- **Sváťa Pulec** – bývalý horolezec, invalida


## Historický kontext

### Divadlo Járy Cimrmana

- autory jsou Svěrák a Smoljak (a Jára Cimrman)
- Reduta, Divadlo na Zábradlí
- bez hereckého vzdělání
  - to že nikdo ze souboru neumí hrát tvoří další vrstvu komiky

- hry mají dvě části:
  - Seminář – pseudovědecký výklad
  - Samotné drama

- mystifikační divadlo
  - Jára Cimrman je zasazen do několika reálných historických událostí
  - jeho činy jsou prezentovány jako realita


- *Akt*, *Vyšetřování ztráty třídní knihy*, *Dobývání severního pólu*, *Afrika*, *České nebe*

## Intertexty

Absurdní divadlo

- Čekání na Godota – Beckett

Divadla malých forem

- SeMaFor
- divadlo ABC

Mystifikace / Zasazení fiktivní postavy do dějinných událostí

- Osudy dobrého vojáka Švejka – Hašek
- Mlýn na mumie (komisař si vymýšlí historky) – Stančík
