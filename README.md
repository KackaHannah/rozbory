# Rozbory

Ahoj! Vítejte v mém repozitáři s rozbory některých děl z maturitního seznamu četby. Díla zpracovávám víc pro své účely, dávám tak větší důraz na historický kontext a autora, než třeba na témata a motivy.

Dokumenty jsou psány ve formátu Markdown, jdou tak jednoduše přečíst přímo na gitlabu. Pro vygenerování pdf souboru použijte [pandoc](https://pandoc.org/) nebo [PDFCreator](https://www.pdfforge.org/online/en/markdown-to-pdf).

Pokud byste chtěli mé dílo rozšířit, stačí si repozitář stáhnout, upravit a vytvořit [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/).