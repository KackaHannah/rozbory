# Hamlet

## Shrnutí děje

Dánský hrad Elsinor, 17. století

Dánskému princi Hamletovi za podivuhodných okolností zemře otec, král. Královna se provdá za králova bratra Claudia. Hamletovi se jednoho večera zjeví duch otce, který mu řekne, že byl zavražděn Claudiem. Hamlet chce svého otce pomstít, a proto sehraje divadlo, při kterém se nový král Claudius prozradí. Posléze Hamlet zjišťuje, že do vraždy otce je zapletena i královna. Královna volá o pomoc a Polonius, který je skrytý v její ložnici, chce přispěchat na pomoc. Hamlet si jej však splete s Claudiem a probodne ho. Ofélie, která Hamleta miluje, zešílí a utopí se. Claudius si uvědomí, že je pro něj Hamlet nebezpečný, a proto jej pošle do Anglie, kde má být popraven. Hamlet však tuto léčku prokoukne a vrátí se zpět domů. Tam na něj čeká Laertes, aby pomstil smrt svou sestru Ofélie a otce Polonia. Proti Hamletovi ho pošle Claudius a rovněž mu dá otrávený kord. Hamlet však kord obrátí proti němu. Zavraždí Laerta i Claudia. Nakonec předává moc vojevůdci Frontibrasovi, který se stane dánským králem. Královna také umře, protože se napila z poháru s otráveným vínem, které mělo být pro Hamleta.

## Postavy

- **Hamlet** – hrdý, z donucení pomstychtivý, má úctu ke svému zemřelému otci, předstírá bláznovství, postupně se však opravdu zblázní, v díle má časté filosofické pasáže, skrz které je možné pozorovat jeho vývoj
- **Claudius** – vrah Hamletova otce, zákeřný, ctižádostivý
- **Gertruda** – Hamletova matka a královna, vnitřní rozpolcení mezi svým mužem Claudiem a synem Hamletem
- **Horatio** – přítel Hamleta, čestný
- **Polonius** – otec Ofélie, komorní, podlézavý oportunista
- **Ofélie** – dcera Polonia, stydlivá (až zbabělá), skrývá své city k Hamletovi
- **Laertes** – loajální syn Polonia a bratr Ofélie, po jejich smrti žije jen pro pomstu

## Historický kontext

- dobové mocenské konflikty
- nenávist anglické společnosti vůči katolíkům
- konec feudalismu

### William Shakespeare

- 1564–1616
- renesanční básník a dramatik
  - Hamlet vzniká v době krize humanismu a renesance, racionalita nenaplnila očekávání
- komedie, historické hry a tragédie
- inspirace v Antice (renesance, obvi)
- *Sen noci svatojánské*, *Othello*, *Bouře*, *Mnoho povyku pro nic*, *Romeo a Julie*
  - inspirace pro *Romeo, Julie a Tma*

Soudobí autoři:

- Cervantes – don Quijote
- Alighieri – Božská komedie
- Petrarca – ty jeho zamilovaný básně a věci
- Villon – Malý a Velký testament, básně

## Intertexty

Rafinovaná pomsta

- Hrabě Monte Christo – Dummas
- Návštěva staré dámy – Dürenmat

Tragická láska

- Utrpení mladého Werthera – Göethe
- (Romeo a Julie – Shakespeare)

Šílenství

- Jindřich IV. – Shakespeare

Claudiova bezohledná touha po moci

- Othello – Shakespeare

Sebevražda

- Utrpení mladého Werthera – Göethe
- Čekání na Godota – Beckett