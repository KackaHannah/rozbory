# Lakomec

## Shrnutí děje

Paříž, 1670

Kleant je zamilovaný do Mariany a svěří se svou lásku své sestře Elišce. Ta je zamilovaná do Valéra. Jejich otce Harpagona zajímají jen peníze. Protože nechce svou dceru prodat za chudého Valéra, který u nich navíc pracuje, provdá ji za starého a bohatého Anselma. Syn je pak oženěn s postarší vdovou. Sám Harpagon si pak vezme Marianu.

Harpagon pozve všechny na večeři, aby se mohli seznámit. Kleant se mezitím rozhodne, že si půjčí peníze, aby si mohl vzít Marinu. Velmi se rozzlobí, když zjistí, že peníze, mu má půjčit jeho otec. Čipera, Kleantův sluha, ukradne peníze Harpagonovi, který si je zakopal. Harpagon začne obviňovat všechny kolem. Čipera prohlásí, že mu je vrátí pouze pod podmínkou, že se Kleant ožení s Marianou.

Vyjde najevo, že Mariana a Valér jsou sourozenci a Anselm je jejich otec. Všichni si mysleli, že zahynuli na moři. Anselm nabídne Harpagonovi, že zaplatí obě svatby. Harpagon je spokojený, že nemusí dát na svatby ani korunu a také, že má zpět své peníze.

## Kompozice

- divadelní hra
- jednoduchá, chronologická
- typizované postavy
  - komedie dell'arte


## Postavy

- **Harpagon** – lakomý starý vdovec, odmítá rozdělit se o své jmění se svými dětmi, které by jej potřebovaly
- **Kleant** – syn Harpagona, mladý kultivovaný muž, zamilovaný do Marianny
- **Eliška** – dcera Harpagona, věří v pravou lásku, zamilovaná do Valéra
- **Marianna** – velmi krásná dívka, skromná a plachá, žije po boku své nemocné matky, zamilovaná do Kleanta
- **Valér** – správce Harpagonova domu, podlézavý donašeč, zamilován do Elišky
- **Anselm** – štědrý a dobrosrdečný šlechtic (zezačátku to neni známo), skrytě otec Marianny a Valéra, vnáší do hry dobrý konec
- **Frosina** – dohazovačka, jako jediná zná všechny poměry v rodině
- **Jakub** – podlý a pomstychtivý sluha/kočí/kuchař ve službě u Harpagona

## Historický kontext

- přelom 17. a 18. století – rozvoj vědy a techniky, přesun hlavního zájmu na člověka
- klasicismus a osvícenství
- později **průmyslová revoluce**, VFR, vyhlášení nezávislosti USA
- vrcholný klasicismus – zaměření na člověka

- *Carlo Goldoni* – Sluha dvou pánů
- *Jean de la Fontaine* – Bajky
- *Pierre Corneille* – Cid

### Molliére

- Jean-Baptiste Poquelin
- herec a ředitel divadelní společnosti
- zesměšňování církve a šlechty, poukazuje na pokrytectví

- *Misantrop*, *Tartuffe*

## Intertexty

Lakomství/Pokrytectví

- Hamlet (postava Claudia) – Shakespeare
- Návštěva staré dámy – Dürenmatt
- Misantrop (zesměšňování hříšných vlastností člověka) – Molliére

Situační komika

- Dekameron – Boccaccio
