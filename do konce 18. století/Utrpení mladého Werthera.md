# Utrpení mladého Werthera

## Shrnutí děje

Malá vesnička Wahlheim v Německu, 2. polovina 18. sotletí

Werther přijíždí na vesnici, aby se zde věnoval zejména malování. Po nějaké době, kdy si užívá samoty, jede Werther na ples v kočáře s několika přítelkyněmi. S nimi jede také Lotta. Mladé, hezké děvče, které má od matčiny smrti na povel celou domácnost. Lotta se má provdat za Alberta, dobrého a poctivého úředníka, který je nyní na cestách. Na plese tančí s Wertherem, který se do Lotty zamiluje.

Od tohoto dne ji navštěvuje skoro denně a podnikají dlouhé procházky. Po čase přijíždí Albert. Werther, který už nesnese déle pobývat v přítomnosti ženy, do které je bezhlavě zamilován, avšak která zároveň patří jinému, vstupuje do služeb na velvyslanectví. Seznamuje se zde s dívkou, která mu připomíná Lottu a tráví s ní nějaký čas. Dopisuje si s Lottou, která je již provdána za Alberta. Mezi ním a úředníky dochází k neshodám, rozhodne se tedy déle nezůstat a místo opouští.

Werther se vrací na venkov, kde se opět začne stýkat s Lottou. Albert začíná trochu žárlit, avšak jeho galantnost a povaha mu nedovoluje nahlas vyslovit své myšlenky. Před Vánocemi odjíždí Albert navštívit známé, Werther se opět schází s Lottou. Vyznává jí svou lásku. Ta poznává, že Werthera nejspíše také miluje, avšak její čest jí nedovolí klamat manžela. Werther odchází domů, napíše dlouhý dopis Lottě a pošle k Albertovi sluhu s prosbou o vypůjčení dvou zbraní, které dle svého tvrzení má na cestu, kterou hodlá vykonat. I když Lotta tuší nějaké neštěstí, postrkována Albertem je sluhovi předá.

Werther se o půlnoci téhož dne těžce zraní střelou z pistole. Ráno je nalezen sluhou, který jde neštěstí oznámit otci Lotty. Před polednem Werther umírá i se svými nenaplněnými city. Je pohřben u dvou lip; v místě, které si sám určil.

## Kompozice

- chronologická výstavba
- **deníkové záznamy a dopisy**

- Klasická vnitřní výstavba
  - Expozice – Werther přichází do Wahlheimu
  - Kolize – seznámení Werthera s Lottou
  - Krize – útěk Werthera z venkova kvůli nešťastné lásce
  - Zvrat – Wertherův návrat, odmítnutí Lottou
  - Závěr – sebevražda

## Postavy

- **Werther** – mladý malíř, velký romantik, bezhlavě zamilován do Lotty, trochu naivní
- **Lotta** – milá způsobná dívka z venkova, skrytě Werthera také miluje, ale chce být věrná Albertovi, k němuž však nechová žádné city
- **Albert** – počestný a vychovaný muž, trochu suchar tbh
- **Vilém** – dobrý přítel Werthera, adresát dopisů od Werthera

## Historický kontext

- preromantismus
  - na pomezí klasicismu a romantismu
  - reakce na chladnou rozumovost renesance – naopak nyní důraz na cit
  - idealizace venkova a běžného nezkaženého člověka

- *Schiller* – An die Freude

### Johann Wolfgang Goethe

- 1749–1832
- jedna z největších osobností světové literatury
- německý básník, prozaik, filosof, vědec, …
- podnět pro napsání Utrpení mladého Werthera byla jeho vlastní nešťastná láska
- hnutí Sturm und Drang
  - *Goethe*, *Schiller*, *Herder*
  - vyjádření umělcovy osobnosti a citů
  - tvůrčí svoboda umělce a odmítnutí společenských konvencí
  - inspirace Shakespeare a Rousseau

- z věkem vyrůstá z rebelie → Výmarská klasika
- *Faust*

## Intertexty

Idealizace venkova a přírody

- Krysař – Vojtěch Dyk
- Máj – Karel Hynek Mácha

Sebevražda

- Hamlet – Shakespeare
- Romeo a Julie – Shakespeare

Láska

- Básně od Petrarcy
- Ossianovy zpěvy
- Frankenstein (monstrum má také nenaplněné city, emoce reflektovány v přírodě) – Mary Shelley
- **Utrpení knížete Sternenhocha** – Klíma
